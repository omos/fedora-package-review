# Fedora package review skeleton

I use this repo to facilitate the process of preparing packages for the
Fedora package review process. It is intended to be used with
[COPR](https://copr.fedorainfracloud.org/) to create test builds and to
track the spec file development before the package(s) is (are) accepted
into Fedora.

## Usage

### Adding package files to the repo

Create a new branch for the new package (or group of packages), based
on the master branch:

```
$ git checkout -b my-package-group master
```

Add subdirectories for each package in the group and put spec files,
patches, etc. in the respective directories:
```
$ mkdir my-package
$ "$EDITOR" my-package/my-package.spec
...
```

### Adding the package(s) to COPR

Create a new COPR project for the new package group. Enable the desired
chroots (at least Rawhide with all relevenat arches recommended) and
check:
* **Project will not be listed on home page** (recommended)
* **Follow Fedora branching** (recommended)
* **Run fedora-review tool for packages in this project**

Add all necessary packages via "New Package", filling in:
* **Package name:** my-package
* **Type:** Git
* **Clone URL:** *URL to this repository or your copy*
* **Committish:** my-package-group *(the branch corresponding to the package group)*
* **Subdirectory:** my-package
* **How to build SRPM from the source:** make srpm

You may also want to check "Auto-rebuild the package?" and
[set up a webhook](https://docs.pagure.org/copr.copr/user_documentation.html#webhooks)
to have the package automatically rebuild when there is a push to the
respective git branch. (But be careful with multiple inter-dependent
packages - it's better to do the builds manually in that case.)

Note that this will only work if all sources refrenced from the spec file
are either in the repo itself or are retrievable by URL using
`spectool -g`.

## Helper tools

The root directory of the repo contains also a few convenience scripts
to work with the skeleton. You can invoke them from the package
subdirectories (e.g. `../fedpkg srpm`) to perform various actions on the
given package.

* `fedpkg` - a simple wrapper for `fedpkg` that ensures all sources are
  fetched and passes `--name` and `--release` explicitly so you don't
  have to do all this manually.
* `srpmlint` - runs `rpmlint` on a freshly generated SRPM. Use this to
  debug rpmlint errors/warnings locally.
